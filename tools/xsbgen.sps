#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2019 Göran Weinholt
#!r6rs

;;; Generate Scheme code from XCB protocol definitions

(import
  (rnrs (6))
  (chibi match)
  (only (srfi :1 lists) filter-map append-map)
  (xitomatl common)
  (wak ssax parsing))

;; XXX: Surely this is available in some library
(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

;;; Parsed versions of the data from the XML files

(define-record-type doc
  (opaque #t)
  (sealed #t)
  (fields x))

(define-record-type x-extension
  (sealed #t)
  (fields header name xname multiword?
          major-version minor-version
          elements))

;; Imports types from another extension. The xproto file is implicitly
;; loaded.
(define-record-type x-decl
  (fields header-name xname))

(define-record-type x-import
  (sealed #t)
  (parent x-decl)
  (fields name))

;; A C-like structure
(define-record-type x-struct
  (sealed #t)
  (parent x-decl)
  (fields name fields))

;; A C-like union
(define-record-type x-union
  (sealed #t)
  (parent x-decl)
  (fields name fields))

;; Represents an encoded event
(define-record-type x-eventstruct
  (sealed #t)
  (parent x-decl)
  (fields name event-type-selectors))

(define-record-type event-type
  (sealed #t)
  (fields extension xge? opcode-min opcode-max))

;; A resource type
(define-record-type x-xidtype
  (sealed #t)
  (parent x-decl)
  (fields name))

;; One of several resource types
(define-record-type x-xidunion          ;XXX: undocumented
  (sealed #t)
  (parent x-decl)
  (fields name types))

;; A C-like enum type
(define-record-type x-enum
  (sealed #t)
  (parent x-decl)
  (fields name items))

;; A type alias
(define-record-type x-typedef
  (sealed #t)
  (parent x-decl)
  (fields newname oldname))

;; An X protocol request
(define-record-type x-request
  (sealed #t)
  (parent x-decl)
  (fields name opcode combine-adjacent?
          fields
          reply-fields))

;; An X protocol event
(define-record-type x-event
  (sealed #t)
  (parent x-decl)
  (fields name number no-sequence-number? xge?
          fields))

;; An X protocol error
(define-record-type x-error
  (sealed #t)
  (parent x-decl)
  (fields name number
          fields))

;; An alias for an event
(define-record-type x-eventcopy
  (sealed #t)
  (parent x-decl)
  (fields name number ref))

;; An alias for an error
(define-record-type x-errorcopy
  (sealed #t)
  (parent x-decl)
  (fields name number ref))

;; These are the things that go into structures and unions

(define-record-type pad
  (fields bytes
          align                         ;XXX: undocumented
          serialize?))

(define-record-type required-start-align ;XXX: undocumented
  (sealed #t)
  (parent pad)
  (fields offset))

(define-record-type var
  (fields name))

(define-record-type field
  (sealed #t)
  (parent var)
  (fields type
          ;; Optional
          enum altenum mask altmask))

(define-record-type fd                  ;out-of-band fd
  (sealed #t)
  (parent var))

(define-record-type listfield
  (sealed #t)
  (parent var)
  (fields type enum mask length-expr))

(define-record-type exprfield
  (sealed #t)
  (parent var)
  (fields type expression))

(define-record-type switch
  (sealed #t)
  (parent var)
  (fields align offset expression clauses))

(define-record-type caseexpr
  (parent var)
  (fields expressions fields))

(define-record-type bitcase-clause
  (sealed #t)
  (parent caseexpr))

(define-record-type case-clause
  (sealed #t)
  (parent caseexpr))

;;; Parse the XML files from xcb/proto

(define assq-ref
  (case-lambda
    ((fields key)
     (cond ((assq key fields) => cadr)
           (else (error 'assq-ref "Key not found" key fields))))
    ((fields key default)
     (cond ((assq key fields) => cadr)
           (else default)))))

;; Binds values from an association list to a variable by the same
;; name as the key. Supports required and optional keys. Checks that
;; no mystery keys are present.
(define-syntax let-attrs
  (syntax-rules ()
    ((_ [alist (req ...) (opt ...)] . body)
     (let ((temp alist))
       (let ((req (assq-ref temp 'req)) ...
             (opt (assq-ref temp 'opt #f)) ...)
         (for-each (lambda (x)
                     (unless (memq (car x) '(req ... opt ...))
                       (error #f "Unrecognized attribute" x alist
                              '(req ... opt ...))))
                   temp)
         . body)))))

(define (parse-expr element)
  (match element
    [('op ('^ attr* ...) expr0 expr1)
     (let-attrs [attr* (op) ()]
       (list (string->symbol op) (parse-expr expr0) (parse-expr expr1)))]
    [('fieldref (? string? name))
     (list 'field name)]
    [('paramref ('^ attr* ...) (? string? name))
     (let-attrs [attr* (type) ()]
       (list 'param type name))]
    [('value value)
     (or (string->number value 10)
         (error 'parse-expr "Unrecognized expression" element))]
    [('bit n)
     (cond ((string->number n 10) =>
            (lambda (n) (list 'bit n)))
           (else
            (error 'parse-expr "Unrecognized expression" element)))]
    [('enumref ('^ attr* ...) item-id)
     (let-attrs [attr* (ref) ()] (list 'enumref (parse-type ref) item-id))]
    [('unop ('^ attr* ...) expr)
     (let-attrs [attr* (op) ()] (list (string->symbol op) (parse-expr expr)))]
    [('sumof ('^ attr* ...))
     (let-attrs [attr* (ref) ()] (list 'sumof ref))]
    [('sumof ('^ attr* ...) expr)
     (let-attrs [attr* (ref) ()] (list 'sumof ref (parse-expr expr)))]
    [('popcount expr)
     (list 'popcount (parse-expr expr))]
    [('listelement-ref)
     (list 'listelement-ref)]
    [x
     (error 'parse-expr "Unrecognized expression" x)]))

(define (parse-clause clause)
  (define (xml-expr? x)
    (guard (exn ((error? exn) #f))
      (parse-expr x)
      #t))
  (match clause
    [((and (or 'case 'bitcase) which) x x* ...)
     (let-values ([(attr* x*)
                   (match x
                     [('^ attr* ...) (values attr* x*)]
                     [x (values '() (cons x x*))])])
       (let ((make (case which
                     ((case) make-case-clause)
                     (else make-bitcase-clause))))
         (let-values ([(expressions fields) (partition xml-expr? x*)])
           (let-attrs [attr* () (name)]
             (make name (map parse-expr expressions)
                   (map parse-field fields))))))]
    [x
     (error 'parse-clause "Unrecognized clause" x)]))

(define (parse-type str)
  (match (string-split str #\:)
    [(name) (string->symbol name)]
    [(header name) (cons header (string->symbol name))]))

(define (parse-field element)
  (match element
    [('pad ('^ attr* ...))
     (let-attrs [attr* () (bytes align serialize)]
       (make-pad (if bytes (string->number bytes) 0)
                 (if align (string->number align) 1)
                 (equal? "true" serialize)))]
    [('field ('^ attr* ...))
     (let-attrs [attr* (type name) (enum altenum mask altmask)]
       (make-field name (parse-type type) enum altenum mask altmask))]
    [('fd ('^ attr* ...))
     (make-fd (assq-ref attr* 'name))]
    [('list ('^ attr* ...) length-expr)
     (let-attrs [attr* (type name) (enum mask)]
       (make-listfield name (parse-type type) enum mask
                       (parse-expr length-expr)))]
    [('list ('^ attr* ...))             ;XXX: undocumented
     ;; This apparently means that the length is implicit and covers
     ;; the rest of the request
     (let-attrs [attr* (type name) (enum mask)]
       (make-listfield name (parse-type type) enum mask #f))]
    [('exprfield ('^ attr* ...) expr)
     (let-attrs [attr* (type name) ()]
       (make-exprfield name (parse-type type) (parse-expr expr)))]
    [('switch ('^ attr* ...) expression ('required_start_align
                                         ('^ align-attr* ...))
              clauses ...)
     ;; Special case that adds an alignment requirement to the struct
     (let-attrs [attr* (name) ()]
       (let-attrs [align-attr* (align offset) ()]
         (make-switch name (string->number align 10)
                      (string->number offset 10)
                      (parse-expr expression)
                      (map parse-clause clauses))))]
    [('switch ('^ attr* ...) expression clauses ...)
     (let-attrs [attr* (name) ()]
       (make-switch name 1 0 (parse-expr expression)
                    (map parse-clause clauses)))]
    [('required_start_align ('^ attr* ...)) ;XXX: undocumented
     (let-attrs [attr* (align) (offset)]
       (make-required-start-align 0 (string->number align 10) #f offset))]
    [('doc . x)
     (make-doc x)]
    [x
     (error 'parse-field "Unknown structure content" x)]))

(define (parse-top-level-element element header xname)
  (match element
    [('import header-name^)
     (make-x-import header xname header-name^)]
    [('struct ('^ attr* ...) fields ...)
     (let-attrs [attr* (name) ()]
       (make-x-struct header xname (string->symbol name) (map parse-field fields)))]
    [('union ('^ attr* ...) fields ...)
     (let-attrs [attr* (name) ()]
       (make-x-union header xname (string->symbol name) (map parse-field fields)))]
    [('xidtype ('^ attr* ...))
     (let-attrs [attr* (name) ()]
       (make-x-xidtype header xname (string->symbol name)))]
    [('xidunion ('^ attr* ...) ('type types) ...)
     (let-attrs [attr* (name) ()]
       (make-x-xidunion header xname (string->symbol name) (map parse-type types)))]
    [('enum ('^ attr* ...) items ...)
     (let-attrs [attr* (name) ()]
       (make-x-enum header xname (string->symbol name)
                    (map (match-lambda
                          [('item ('^ attr* ...) expr)
                           (cons (assq-ref attr* 'name)
                                 (parse-expr expr))]
                          [('item ('^ attr* ...) #f)
                           (cons (assq-ref attr* 'name) #f)]
                          [('doc . x)
                           (make-doc x)]
                          [x (error 'parse-top-level-element
                                    "Unknown enum field" x)])
                         items)))]
    [('typedef ('^ attr* ...))
     (let-attrs [attr* (oldname newname) ()]
       (make-x-typedef header xname (parse-type newname) (parse-type oldname)))]
    [('request ('^ attr* ...) rest* ...)
     (let-attrs [attr* (name opcode) (combine-adjacent)]
       (make-x-request header xname name (string->number opcode 10)
                       (equal? "true" combine-adjacent)
                       (map parse-field (remp (lambda (x) (eq? (car x) 'reply))
                                              rest*))
                       (cond ((assq 'reply rest*) =>
                              (lambda (field*) (map parse-field (cdr field*))))
                             (else '()))))]
    [('event ('^ attr* ...) fields ...)
     (let-attrs [attr* (name number) (no-sequence-number xge)]
       (make-x-event header xname name (string->number number 10)
                     (equal? "true" no-sequence-number)
                     (equal? "true" xge)
                     (map parse-field fields)))]
    [('error ('^ attr* ...) fields ...)
     (let-attrs [attr* (name number) ()]
       (make-x-error header xname name (string->number number 10) (map parse-field fields)))]
    [('eventcopy ('^ attr* ...))
     (let-attrs [attr* (name number ref) ()]
       (make-x-eventcopy header xname name (string->number number 10) ref))]
    [('errorcopy ('^ attr* ...))
     (let-attrs [attr* (name number ref) ()]
       (make-x-errorcopy header xname name (string->number number 10) ref))]
    [('eventstruct ('^ attr* ...) event-type-selector+ ...)
     (let ((selectors
            (map (match-lambda
                  [('allowed ('^ attr* ...))
                   (let-attrs [attr* (extension xge opcode-min
                                                opcode-max) ()]
                     (make-event-type extension (equal? xge "true")
                                      opcode-min opcode-max))])
                 event-type-selector+)))
       (let-attrs [attr* (name) ()]
         (make-x-eventstruct header xname (parse-type name) selectors)))]
    [x
     (error 'parse-top-level-element
            "Unknown top-level element" element header xname)]))

(define (parse-xml filename)
  (define (parse attr* element*)
    (let-attrs [attr* (header) (extension-xname
                                extension-name extension-multiword
                                major-version minor-version)]
      (make-x-extension header extension-name extension-xname
                        (equal? extension-multiword "true")
                        major-version minor-version
                        (map (lambda (x)
                               (parse-top-level-element x header extension-name))
                             element*))))
  (match (call-with-input-file filename
           (lambda (p)
             (ssax:xml->sxml p '())))
    [('*TOP* ('*PI* . _)
             ('xcb ('^ attr* ...) element* ...))
     (parse attr* element*)]
    [('*TOP* ('xcb ('^ attr* ...) element* ...))
     ;; XXX: missing <?xml ...?>
     (parse attr* element*)]
    [x (error 'parse-xlm "Unrecognized xml" x)]))

;;; Code generation

;; Should generate the same names for constants as libxcb.
(define (upper-case-name str)
  (call-with-string-output-port
    (lambda (p)
      (let lp ((i 0) (previous-lc #f))
        (unless (fx=? i (string-length str))
          (let* ((c (string-ref str i))
                 (lc (char-lower-case? c)))
            (when (and previous-lc (not lc))
              (put-char p #\_))
            (put-char p (char-upcase c))
            (lp (fx+ i 1) lc)))))))

(define (xcb-constant-name . x*)
  (string->symbol
   (call-with-string-output-port
     (lambda (p)
       (put-string p "XCB_")
       (let lp ((x* x*))
         (unless (null? x*)
           (when (car x*)
             (put-string p (upper-case-name (car x*)))
             (unless (null? (cdr x*))
               (put-string p "_")))
           (lp (cdr x*))))))))

;; This is used for structures
(define (schemely-name str)
  (call-with-string-output-port
    (lambda (p)
      (let lp ((i 0) (previous-lc #f))
        (unless (fx=? i (string-length str))
          (let* ((c (string-ref str i))
                 (lc (char-lower-case? c)))
            (when (and previous-lc (not lc))
              (put-char p #\-))
            (put-char p (char-downcase c))
            (lp (fx+ i 1) lc)))))))

;; Variables in structures, requests and replies, etc
(define (var-scheme-name var)
  (cond
    ((equal? (var-name var) "do_not_propogate_mask")
     (var-scheme-name (make-var "do_not_propagate_mask")))
    (else
     (string->symbol
      (call-with-string-output-port
        (lambda (p)
          (string-for-each
           (lambda (c)
             (if (eqv? c #\_)
                 (put-char p #\-)
                 (put-char p c)))
           (var-name var))))))))

(define (struct-putter-name x)
  (let ((xname (x-decl-xname x)))
    (string->symbol
     (string-append "put-"
                    (if xname (string-append (string-downcase xname) ":") "")
                    (schemely-name (symbol->string (x-struct-name x)))))))

(define (struct-field-ref-name struct field)
  (let ((xname (x-decl-xname struct)))
    (string->symbol
     (string-append "xsb-"
                    (if xname (string-append (string-downcase xname) ":") "")
                    (schemely-name (symbol->string (x-struct-name struct)))
                    "-"
                    (symbol->string (var-scheme-name field))))))

(define (request-reply-ref-name struct field)
  (let ((xname (x-decl-xname struct)))
    (string->symbol
     (string-append "xsb-"
                    (if xname (string-append (string-downcase xname) ":") "")
                    (schemely-name (x-request-name struct))
                    "-"
                    (symbol->string (var-scheme-name field))))))

(define (request-putter-name x)
  (let ((xname (x-decl-xname x)))
    (string->symbol
     (string-append "putreq-"
                    (if xname (string-append (string-downcase xname) ":") "")
                    (schemely-name (x-request-name x))))))

(define (request-call-name x)
  (let ((xname (x-decl-xname x)))
    (string->symbol
     (string-append "xsb-"
                    (if xname (string-append (string-downcase xname) ":") "")
                    (schemely-name (x-request-name x))))))

(define (request-opcode-name x)
  (xcb-constant-name (x-decl-xname x) (x-request-name x)))

(define (header->library-name x)
  `(xsb ,(string->symbol x)))

(define (enum-scheme-name type item-name)
  (xcb-constant-name (x-decl-xname type)
                     (symbol->string (x-enum-name type))
                     item-name))

(define (codegen extension imported-extensions)
  ;; Code generation state

  (define libname (header->library-name (x-extension-header extension)))
  (define exports '())
  (define imports '((rnrs) (xsb private connection)))
  (define top-level '())
  (define (emit-import x)
    (unless (member x imports)
      (set! imports (cons x imports))))
  (define (emit-export x)
    (set! exports (cons x exports)))
  (define (emit x)
    (set! top-level (cons x top-level)))

  ;; State for types. All types go into the hashtable under '("header"
  ;; . type) and 'type.
  (define types (make-hashtable (lambda (x)
                                  (if (pair? x)
                                      (bitwise-ior (string-hash (car x))
                                                   (symbol-hash (cdr x)))
                                      (symbol-hash x)))
                                equal?))
  (define (register-type header-name type-name x-record)
    ;; Namespacing is weird. The header= attribute in the <xcb>
    ;; element defines a namespace for types, which can be referred to
    ;; using the "header:type" notation, but types are also available
    ;; without the "header:" prefix.
    (hashtable-set! types (cons header-name type-name) x-record)
    (hashtable-set! types type-name x-record))
  (define (lookup-type name)
    (define (return type)
      (display "=> ")
      (write type) (newline)
      (if (x-typedef? type)
          (lookup-type (x-typedef-oldname type))
          type))
    (write (list 'lookup-type name)) (newline)
    (cond
      ((memq name '(CARD8 CARD16 CARD32 CARD64 INT8 INT16 INT32 BOOL BYTE
                          float double fd))
       => car)
      ((memq name '(char void)) => car) ;list element types
      ((pair? name)
       (cond ((hashtable-ref types name #f) => return)
             (else
              (assertion-violation 'lookup-type "Unknown type" name))))
      ((hashtable-ref types (cons (x-extension-header extension) name) #f) => return)
      ((hashtable-ref types (cons "xproto" name) #f) => return)
      ((hashtable-ref types name #f) => return)
      (else
       (assertion-violation 'lookup-type "Unknown type" name))))

  ;; Code generation

  (define (cg-expr x)
    (match x
      [(? number? x) x]
      [('bit n) (bitwise-arithmetic-shift-left 1 n)]
      [('field name)
       (if (equal? name "string_len")
           '(string-length string)      ;yep.
           (var-scheme-name (make-var name)))]
      [((and (or '+ '- '*) operator) op1 op2)
       `(,operator ,(cg-expr op1) ,(cg-expr op2))]
      [('/ op1 op2)
       ;; XXX: If this should handle negative numbers then it should
       ;; be quotient instead of fxdiv
       ;; (emit-import '(rnrs r5rs))
       `(fxdiv ,(cg-expr op1) ,(cg-expr op2))]
      [('<< op1 op2)
       `(bitwise-arithmetic-shift-left ,(cg-expr op1) ,(cg-expr op2))]
      [('& op1 op2)
       `(bitwise-and ,(cg-expr op1) ,(cg-expr op2))]
      [('~ op)
       `(bitwise-not ,(cg-expr op))]
      [('popcount op)
       `(bitwise-bit-count ,(cg-expr op))]
      [('enumref type-name item-name)
       (let ((type (lookup-type type-name)))
         (xcb-constant-name (x-decl-xname type)
                            (symbol->string (x-enum-name type))
                            item-name))]
      [('param type name)
       (var-scheme-name (make-var name))]
      [#f #f]
      [_ (error 'cg-expr "Unimplemented expression" x)]))

  (define (cg-enum x)
    (let lp ((prev #f) (items (x-enum-items x)))
      (unless (null? items)
        (match items
          [((name . expr) . items)
           (let ((name (enum-scheme-name x name)))
             (emit-export name)
             (emit `(define ,name ,(if (not expr)
                                       `(+ ,prev 1)
                                       (cg-expr expr)))))
           (lp name items)]
          [((? doc?) . items)
           (lp prev items)]
          [_ (error 'cg-enum "Unimplemented expression" x)]))))

  ;; Emit code for a single field element, which may be a simple type
  ;; or a structure (but not a list).
  (define (cg-put-field-element type code)
    (write `(cg-put-field-element ,type ,code))
    (newline)
    (case type
      ((CARD8)
       `((_ (begin (put-u8 p ,code) #f))
         (offset (fx+ offset 1))))
      ((INT8 BYTE)
       `((_ (begin (put-u8 p (fxand ,code #xff)) #f))
         (offset (fx+ offset 1))))
      ((BOOL)
       `((_ (begin (put-u8 p (if ,code 1 0)) #f))
         (offset (fx+ offset 1))))
      ((CARD16)
       `((_ (begin
              (bytevector-u16-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 2)
              #f))
         (offset (fx+ offset 2))))
      ((INT16)
       `((_ (begin
              (bytevector-s16-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 2)
              #f))
         (offset (fx+ offset 2))))
      ((CARD32 fd)
       `((_ (begin
              (bytevector-u32-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 4)
              #f))
         (offset (fx+ offset 4))))
      ((INT32)
       `((_ (begin
              (bytevector-s32-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 4)
              #f))
         (offset (fx+ offset 4))))
      ((CARD64)
       `((_ (begin
              (bytevector-u64-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 8)
              #f))
         (offset (fx+ offset 8))))
      ((float)
       `((_ (begin
              (bytevector-ieee-single-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 4)
              #f))
         (offset (fx+ offset 4))))
      ((double)
       `((_ (begin
              (bytevector-ieee-double-native-set! bvtemp 0 ,code)
              (put-bytevector p bvtemp 0 8)
              #f))
         (offset (fx+ offset 8))))
      (else
       (cond ((or (x-xidtype? type) (x-xidunion? type))
              `((v ,code)
                (_ (begin
                     (bytevector-u32-native-set! bvtemp 0 v)
                     (put-bytevector p bvtemp 0 4)
                     #f))
                (offset (fx+ offset 4))))
             ((x-struct? type)
              `((offset (fx+ offset
                             (,(struct-putter-name type) p ,code)))))
             ((x-eventstruct? type)
              `((v ,code)
                (_ (begin
                     (put-bytevector p v 0 32)
                     #f))
                (offset (fx+ offset 32))))
             ((x-union? type)
              `((v ,code)
                (_ (begin (error #f "TODO" ,(x-union-name type))))
                (offset (fx+ offset 'todo))))
             (else
              (error 'cg-put-field-element "Unrecognized field type" type))))))

  (define (cg-list-length-expr fields f)
    (match (listfield-length-expr f)
      [('sumof ref)
       (list 'sumof ref)]
      [('sumof ref expr)
       (list 'sumof ref (cg-expr expr))]
      [x (cg-expr x)]))

  ;; Emit code for writing the fields of a structure or request to a
  ;; binary output port.
  (define (cg-put-fields fields)
    (define (cg-put-field x)
      (write `(cg-put-field ,x)) (newline)
      (cond
        ((field? x)
         (let ((type (lookup-type (field-type x))))
           (cg-put-field-element type (var-scheme-name x))))
        ((fd? x)
         ;; TODO: Pass the fd over the local socket
         (cg-put-field-element 'fd (var-scheme-name x)))
        ((exprfield? x)
         (let ((type (lookup-type (exprfield-type x))))
           (cg-put-field-element type (cg-expr (exprfield-expression x)))))
        ((listfield? x)
         ;; TODO: handle mask
         (let ((type (lookup-type (listfield-type x))))
           (case type
             ((char void)
              ;; char and void lists are represented as bytevectors
              `((len ,(cg-list-length-expr fields x))
                (_ (begin (put-bytevector p ,(var-scheme-name x) 0 len)
                          #f))
                (offset (fx+ offset len))))
             (else
              ;; All other list fields are represented as lists of
              ;; simpler types. There must be at least as many
              ;; elements as the length field indicates.
              `((sum-len (let lp ((x* ,(var-scheme-name x)) (sum-len 0) (i 0))
                           (if (null? x*)
                               sum-len
                               (let ((len ,(cg-put-field-element type '(car x*))))
                                 (lp (cdr x*) (fx+ sum-len len) (fx+ i 1))))))
                (offset (fx+ offset sum-len)))))))
        ((switch? x)
         ;; TODO: handle align and offset
         `((_ (let ((v ,(cg-expr (switch-expression x))))
                ,@(map
                   (lambda (c)
                     `(when (and
                              ,@(map (if (bitcase-clause? c)
                                         (lambda (expr)
                                           `(not
                                             (eqv? 0 (bitwise-and v ,(cg-expr expr)))))
                                         (lambda (expr)
                                           `(eqv? ,(cg-expr expr) v)))
                                     (caseexpr-expressions c)))
                        ,(let f ((fields (caseexpr-fields c)))
                           ;; (write fields) (newline)
                           ;; FIXME: handle required-start-align
                           (if (null? fields)
                               'offset
                               `(let* (,@(if (var? (car fields))
                                             `((,(var-scheme-name (car fields))
                                                (assq-ref ,(var-scheme-name x)
                                                          ',(var-scheme-name
                                                             (car fields)))))
                                             '())
                                       ,@(cg-put-field (car fields)))
                                  ,(f (cdr fields)))))))
                   (switch-clauses x))
                #f))))
        ((pad? x)                   ;insert padding or alignment
         ;; TODO: required_start_align offset="..."
         (cond ((not (eqv? 1 (pad-align x)))
                `((offset^ (fxand (fx+ offset ,(fx- (pad-align x) 1))
                                  ,(fx- (pad-align x))))
                  (_ (begin
                       (put-bytevector p (make-bytevector
                                          (fx- offset^ offset) 0))
                       #f))
                  (offset offset^)))
               ((eqv? (pad-bytes x) 1)
                `((_ (begin (put-u8 p 0) #f))
                  (offset (fx+ offset 1))))
               (else
                `((_ (begin
                       (put-bytevector p (make-bytevector ,(pad-bytes x) 0))
                       #f))
                  (offset (fx+ offset ,(pad-bytes x)))))))
        ((doc? x) '())
        (else
         (display "unsupported field!\n")
         `((_ (begin
                (write (list "Unsupported field member"
                             ,(call-with-string-output-port
                                (lambda (p) (write x p)))))
                (newline)))))))
    ;; For each field we generate code that writes it to a binary
    ;; output port and then returns the length of the data.
    (let ((args (filter-map (lambda (x)
                              (and (var? x)
                                   (not (exprfield? x))
                                   (var-scheme-name x)))
                            fields))
          (serializer
           (if (null? fields)
               0
               `(let* ((offset 0)
                       (bvtemp (make-bytevector 8))
                       ,@(let f ((fields fields))
                           (if (null? fields)
                               '()
                               `(,@(cg-put-field (car fields))
                                 ,@(f (cdr fields))))))
                  offset))))
      (values args serializer)))

  (define (field->sexpr struct/req fields f)
    (define (simple-type->sexpr name type ref/set)
      (cond ((x-xidtype? type)
             `(,name (xidtype ,(x-xidtype-name type)) ,@ref/set))
            ((x-xidunion? type)
             `(,name (xidunion ,(x-xidunion-name type)) ,@ref/set))
            ((x-struct? type)
             `(,name (struct ,(x-struct-name type) ,@ref/set)))
            ((x-eventstruct? type)
             ;; FIXME: schemely name
             `(,name (eventstruct ,(x-eventstruct-name type))
                     ,@ref/set))
            ((x-union? type)
             `(,name (union ,(x-union-name type)) ,@ref/set))
            ((symbol? type)
             `(,name (scalar ,type) ,@ref/set))
            (else
             (error 'field->sexpr "Unknown simple field type" type))))
    (cond
      ((var? f)
       (let ((name (var-scheme-name f))
             (ref/set (cond ((x-struct? struct/req)
                             (list (struct-field-ref-name struct/req f)))
                            ((x-request? struct/req)
                             ;; XXX: this is from the reply
                             (list (request-reply-ref-name struct/req f)))
                            (else
                             '()))))
         (cond
           ((field? f)
            (let ((type (lookup-type (field-type f))))
              (simple-type->sexpr name type ref/set)))
           ((fd? f)
            `(,name (fd) ,@ref/set))
           ((exprfield? f)
            (let ((type (lookup-type (exprfield-type f))))
              `(,name (scalar ,type ,(cg-expr (exprfield-expression f)))
                      ,@ref/set)))

           ((listfield? f)
            (let ((type (lookup-type (listfield-type f))))
              `(,name (list ,(cadr (simple-type->sexpr name type '()))
                            ,(cg-list-length-expr fields f))
                      ,@ref/set)))
           ((switch? f)
            `(,name
              (switch ,(cg-expr (switch-expression f))
                      (align ,(switch-align f))
                      (offset ,(switch-offset f))
                      ,@(map (lambda (c)
                               `((and ,@(map (if (bitcase-clause? c)
                                                 (lambda (expr)
                                                   `(bitcase ,(cg-expr expr)))
                                                 (lambda (expr)
                                                   `(case ,(cg-expr expr))))
                                             (caseexpr-expressions c)))
                                 => ,@(map (lambda (f)
                                             (cond ((var? f)
                                                    (var-scheme-name f))
                                                   ((pad? f)
                                                    `(pad ,(pad-bytes f)
                                                          ,(pad-align f)))
                                                   (else
                                                    (error 'field->sexpr
                                                           "Unknown caseexpr field"
                                                           f))))
                                           (caseexpr-fields c))))
                             (switch-clauses f)))
              ,@ref/set))
           (else
            (error 'field->sexpr "Unknown var" struct/req fields f)))))
      ((pad? f)
       (cond ((not (eqv? (pad-align f) 1))
              (assert (eqv? (pad-bytes f) 0))
              `(_ (align ,(pad-align f))))
             (else
              `(_ (pad ,(pad-bytes f))))))
      (else
       (error 'field->sexpr "Unknown field type" struct/req fields f))))

  ;; Emit code for a structure.
  (define (cg-struct x)
    (let ((name (struct-putter-name x)))
      (emit-export name)
      (let-values ([(args serializer) (cg-put-fields (x-struct-fields x))])
        (emit `(define (,name p ,@args)
                 ,serializer))))
    (emit-import '(xsb private syntax))
    (emit `(define-struct ,(x-struct-name x)
             ,@(map (lambda (f)
                      (let ((sexpr (field->sexpr x (x-struct-fields x) f)))
                        (match sexpr
                          [(_name _type ref)
                           (emit-export ref)]
                          [_ #f])
                        sexpr))
                    (filter (lambda (x) (not (doc? x)))
                            (x-struct-fields x))))))

  ;; Emit code for the fields of the request. The fields as given in
  ;; the XML are not all fields. The request header is packed as
  ;; "CCS", where the first u8 is the opcode or extension, the second
  ;; u8 is the first field and the u16 is the length of the whole
  ;; request divided by four.
  (define (cg-request x)
    (let* ((putname (request-putter-name x))
           (reqname (request-call-name x))
           (opcodename (request-opcode-name x))
           (fields
            (cond
              ((null? (x-request-fields x))
               `(,(make-exprfield "opcode" 'CARD8 (x-request-opcode x))
                 ,(make-exprfield "pad" 'CARD8 0)
                 ,(make-pad 2 1 #f)))   ;length
              ((equal? (x-extension-header extension) "xproto")
               `(,(make-exprfield "opcode" 'CARD8 (x-request-opcode x))
                 ,(car (x-request-fields x))
                 ,(make-pad 2 1 #f)     ;length
                 ,@(cdr (x-request-fields x))
                 ,(make-pad 0 4 #f)))
              (else
               `(,(make-field "extension-number" 'CARD8 #f #f #f #f)
                 ,(make-exprfield "opcode" 'CARD8 (x-request-opcode x))
                 ,(make-pad 2 1 #f)     ;length
                 ,@(x-request-fields x)
                 ,(make-pad 0 4 #f))))))
      (let-values ([(args serializer) (cg-put-fields fields)])
        (emit-export opcodename)
        (emit `(define ,opcodename ,(x-request-opcode x)))
        (emit-export putname)
        (emit `(define (,putname p ,@args)
                 ,serializer))
        (emit-export reqname)
        (emit `(define (,reqname c ,@args)
                 (let ((x (call-with-bytevector-output-port
                            (lambda (p)
                              (,putname p ,@args)))))
                   (bytevector-u16-native-set! x 2 (fxdiv (bytevector-length x) 4))
                   (put-bytevector (xsb-connection-oport c) x))))
        (emit-import '(xsb private syntax))
        (emit `(define-request (,reqname c ,@args)
                 ,@(map (lambda (f)
                          (let ((sexpr (field->sexpr #f fields f)))
                            sexpr))
                        (filter (lambda (x) (not (doc? x))) fields))))
        (unless (null? (x-request-reply-fields x))
          (emit `(define-reply ,(string->symbol (schemely-name (x-request-name x)))
                   ,@(map (lambda (f) (field->sexpr x (x-request-reply-fields x) f))
                          (filter (lambda (x) (not (doc? x)))
                                  (x-request-reply-fields x)))))))))

  ;; Generates Scheme code for structs, requests, etc
  (define (cg x)
    (write `(cg ,x))
    (newline)
    (cond
      ((x-import? x) (emit-import (header->library-name (x-import-name x))))
      ((x-struct? x) (cg-struct x))
      ((x-xidtype? x) #f)
      ((x-xidunion? x) #f)
      ((x-enum? x) (cg-enum x))
      ((x-typedef? x) #f)
      ((x-request? x) (cg-request x))
      (else
       (display "IGNORED!\n"))))

  ;; All defined types are recorded, e.g. '("xproto" . "WINDOW") =>
  ;; #<xidtype>
  (define (process-type x header-name)
    (write `(process-type ,x ,header-name))
    (newline)
    (cond
      ((x-struct? x)
       (register-type header-name (x-struct-name x) x))
      ((x-union? x)
       (register-type header-name (x-union-name x) x))
      ((x-xidtype? x)
       (register-type header-name (x-xidtype-name x) x))
      ((x-xidunion? x)
       (register-type header-name (x-xidunion-name x) x))
      ((x-typedef? x)
       (register-type header-name (x-typedef-newname x) x))
      ((x-enum? x)
       (register-type header-name (x-enum-name x) x))
      ((x-eventstruct? x)
       (register-type header-name (x-eventstruct-name x) x))))

  ;; Process the imported extensions to get type definitions
  (for-each (lambda (ext)
              (for-each (lambda (x)
                          (process-type x (x-extension-header ext)))
                        (x-extension-elements ext)))
            imported-extensions)
  (for-each (lambda (x)
              (process-type x (x-extension-header extension))
              (cg x))
            (x-extension-elements extension))
  `(library ,libname
     (export ,@(reverse exports))
     (import ,@(reverse imports))
     (define (assq-ref lst x)
       (cond ((assq x lst) => cdr)
             (else (assertion-violation #f "Missing key" lst x))))
     ,@(reverse top-level)))

(define (write-r6rs-library input-filename output-filename code)
  (call-with-port (open-file-output-port output-filename
                                         (file-options no-fail)
                                         (buffer-mode block)
                                         (make-transcoder (utf-8-codec)
                                                          (eol-style none)))
    (lambda (p)
      (display ";; Automatically generated by xsbgen.sps from:\n" p)
      (display ";; " p)
      (display input-filename p)
      (newline p)
      (display "#!r6rs\n" p)
      (pretty-print code p))))

(define (gather-imports srcdir extension)
  (filter-map
   (lambda (x)
     (and (x-import? x)
          (parse-xml (string-append srcdir "/" (x-import-name x)
                                    ".xml"))))
   (x-extension-elements extension)))

(define (generate-r6rs-library srcdir input-filename output-filename)
  (let* ((extension (parse-xml input-filename))
         (imports (gather-imports srcdir extension))
         (code (codegen extension imports)))
    (write-r6rs-library input-filename output-filename code)
    code))

(match (command-line)
  [(_ "--r6rs" srcdir xml "-o" sls)
   (generate-r6rs-library srcdir xml sls)])
