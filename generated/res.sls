;; Automatically generated by xsbgen.sps from:
;; xcb/proto/src/res.xml
#!r6rs
(library (xsb res)
  (export put-res:client xsb-res:client-resource-base
   xsb-res:client-resource-mask put-res:type
   xsb-res:type-resource-type xsb-res:type-count
   XCB_RES_CLIENT_ID_MASK_CLIENT_XID
   XCB_RES_CLIENT_ID_MASK_LOCAL_CLIENT_PID
   put-res:client-id-spec xsb-res:client-id-spec-client
   xsb-res:client-id-spec-mask put-res:client-id-value
   xsb-res:client-id-value-value xsb-res:client-id-value-length
   put-res:resource-id-spec xsb-res:resource-id-spec-resource
   xsb-res:resource-id-spec-type put-res:resource-size-spec
   xsb-res:resource-size-spec-ref-count
   xsb-res:resource-size-spec-use-count
   xsb-res:resource-size-spec-bytes put-res:resource-size-value
   xsb-res:resource-size-value-cross-references
   xsb-res:resource-size-value-num-cross-references
   XCB_RES_QUERY_VERSION putreq-res:query-version
   xsb-res:query-version XCB_RES_QUERY_CLIENTS
   putreq-res:query-clients xsb-res:query-clients
   XCB_RES_QUERY_CLIENT_RESOURCES
   putreq-res:query-client-resources
   xsb-res:query-client-resources
   XCB_RES_QUERY_CLIENT_PIXMAP_BYTES
   putreq-res:query-client-pixmap-bytes
   xsb-res:query-client-pixmap-bytes XCB_RES_QUERY_CLIENT_IDS
   putreq-res:query-client-ids xsb-res:query-client-ids
   XCB_RES_QUERY_RESOURCE_BYTES putreq-res:query-resource-bytes
   xsb-res:query-resource-bytes)
  (import
    (xsb private connection)
    (rnrs)
    (xsb xproto)
    (xsb private syntax))
  (define (assq-ref lst x)
    (cond
      [(assq x lst) => cdr]
      [else (assertion-violation #f "Missing key" lst x)]))
  (define (put-res:client p resource-base resource-mask)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 resource-base)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 resource-mask)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)])
      offset))
  (define-struct
    Client
    (resource-base (scalar CARD32) xsb-res:client-resource-base)
    (resource-mask
      (scalar CARD32)
      xsb-res:client-resource-mask))
  (define (put-res:type p resource-type count)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [v resource-type]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 v)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 count)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)])
      offset))
  (define-struct
    Type
    (resource-type (xidtype ATOM) xsb-res:type-resource-type)
    (count (scalar CARD32) xsb-res:type-count))
  (define XCB_RES_CLIENT_ID_MASK_CLIENT_XID 1)
  (define XCB_RES_CLIENT_ID_MASK_LOCAL_CLIENT_PID 2)
  (define (put-res:client-id-spec p client mask)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 client)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 mask)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)])
      offset))
  (define-struct
    ClientIdSpec
    (client (scalar CARD32) xsb-res:client-id-spec-client)
    (mask (scalar CARD32) xsb-res:client-id-spec-mask))
  (define (put-res:client-id-value p spec length value)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [offset (fx+ offset (put-res:client-id-spec p spec))]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 length)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [sum-len (let lp ([x* value] [sum-len 0] [i 0])
                      (if (null? x*)
                          sum-len
                          (let ([len ((_ (begin
                                           (bytevector-u32-native-set!
                                             bvtemp
                                             0
                                             (car x*))
                                           (put-bytevector p bvtemp 0 4)
                                           #f))
                                       (offset (fx+ offset 4)))])
                            (lp (cdr x*) (fx+ sum-len len) (fx+ i 1)))))]
           [offset (fx+ offset sum-len)])
      offset))
  (define-struct
    ClientIdValue
    (spec (struct ClientIdSpec xsb-res:client-id-value-spec))
    (length (scalar CARD32) xsb-res:client-id-value-length)
    (value
      (list (scalar CARD32) (fxdiv length 4))
      xsb-res:client-id-value-value))
  (define (put-res:resource-id-spec p resource type)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 resource)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 type)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)])
      offset))
  (define-struct
    ResourceIdSpec
    (resource (scalar CARD32) xsb-res:resource-id-spec-resource)
    (type (scalar CARD32) xsb-res:resource-id-spec-type))
  (define (put-res:resource-size-spec p spec bytes ref-count
           use-count)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [offset (fx+ offset (put-res:resource-id-spec p spec))]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 bytes)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 ref-count)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 use-count)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)])
      offset))
  (define-struct ResourceSizeSpec
    (spec
      (struct ResourceIdSpec xsb-res:resource-size-spec-spec))
    (bytes (scalar CARD32) xsb-res:resource-size-spec-bytes)
    (ref-count
      (scalar CARD32)
      xsb-res:resource-size-spec-ref-count)
    (use-count
      (scalar CARD32)
      xsb-res:resource-size-spec-use-count))
  (define (put-res:resource-size-value p size
           num-cross-references cross-references)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [offset (fx+ offset (put-res:resource-size-spec p size))]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 num-cross-references)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [sum-len (let lp ([x* cross-references] [sum-len 0] [i 0])
                      (if (null? x*)
                          sum-len
                          (let ([len ((offset
                                        (fx+ offset
                                             (put-res:resource-size-spec
                                               p
                                               (car x*)))))])
                            (lp (cdr x*) (fx+ sum-len len) (fx+ i 1)))))]
           [offset (fx+ offset sum-len)])
      offset))
  (define-struct
    ResourceSizeValue
    (size
      (struct ResourceSizeSpec xsb-res:resource-size-value-size))
    (num-cross-references
      (scalar CARD32)
      xsb-res:resource-size-value-num-cross-references)
    (cross-references
      (list (struct ResourceSizeSpec) num-cross-references)
      xsb-res:resource-size-value-cross-references))
  (define XCB_RES_QUERY_VERSION 0)
  (define (putreq-res:query-version p extension-number
           client-major client-minor)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p extension-number) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 0) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)]
           [_ (begin (put-u8 p client-major) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p client-minor) #f)]
           [offset (fx+ offset 1)]
           [offset^ (fxand (fx+ offset 3) -4)]
           [_ (begin
                (put-bytevector p (make-bytevector (fx- offset^ offset) 0))
                #f)]
           [offset offset^])
      offset))
  (define (xsb-res:query-version c extension-number
           client-major client-minor)
    (let ([x (call-with-bytevector-output-port
               (lambda (p)
                 (putreq-res:query-version
                   p
                   extension-number
                   client-major
                   client-minor)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request
    (xsb-res:query-version
      c
      extension-number
      client-major
      client-minor)
    (extension-number (scalar CARD8)) (opcode (scalar CARD8 0))
    (_ (pad 2)) (client-major (scalar CARD8))
    (client-minor (scalar CARD8)) (_ (align 4)))
  (define-reply
    query-version
    (_ (pad 1))
    (server-major
      (scalar CARD16)
      xsb-res:query-version-server-major)
    (server-minor
      (scalar CARD16)
      xsb-res:query-version-server-minor))
  (define XCB_RES_QUERY_CLIENTS 1)
  (define (putreq-res:query-clients p)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p 1) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 0) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)])
      offset))
  (define (xsb-res:query-clients c)
    (let ([x (call-with-bytevector-output-port
               (lambda (p) (putreq-res:query-clients p)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request
    (xsb-res:query-clients c)
    (opcode (scalar CARD8 1))
    (pad (scalar CARD8 0))
    (_ (pad 2)))
  (define-reply query-clients (_ (pad 1))
    (num-clients
      (scalar CARD32)
      xsb-res:query-clients-num-clients)
    (_ (pad 20))
    (clients
      (list (struct Client) num-clients)
      xsb-res:query-clients-clients))
  (define XCB_RES_QUERY_CLIENT_RESOURCES 2)
  (define (putreq-res:query-client-resources p
           extension-number xid)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p extension-number) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 2) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 xid)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [offset^ (fxand (fx+ offset 3) -4)]
           [_ (begin
                (put-bytevector p (make-bytevector (fx- offset^ offset) 0))
                #f)]
           [offset offset^])
      offset))
  (define (xsb-res:query-client-resources c extension-number
           xid)
    (let ([x (call-with-bytevector-output-port
               (lambda (p)
                 (putreq-res:query-client-resources
                   p
                   extension-number
                   xid)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request (xsb-res:query-client-resources c extension-number xid)
    (extension-number (scalar CARD8)) (opcode (scalar CARD8 2))
    (_ (pad 2)) (xid (scalar CARD32)) (_ (align 4)))
  (define-reply query-client-resources (_ (pad 1))
    (num-types
      (scalar CARD32)
      xsb-res:query-client-resources-num-types)
    (_ (pad 20))
    (types
      (list (struct Type) num-types)
      xsb-res:query-client-resources-types))
  (define XCB_RES_QUERY_CLIENT_PIXMAP_BYTES 3)
  (define (putreq-res:query-client-pixmap-bytes p
           extension-number xid)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p extension-number) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 3) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 xid)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [offset^ (fxand (fx+ offset 3) -4)]
           [_ (begin
                (put-bytevector p (make-bytevector (fx- offset^ offset) 0))
                #f)]
           [offset offset^])
      offset))
  (define (xsb-res:query-client-pixmap-bytes c
           extension-number xid)
    (let ([x (call-with-bytevector-output-port
               (lambda (p)
                 (putreq-res:query-client-pixmap-bytes
                   p
                   extension-number
                   xid)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request (xsb-res:query-client-pixmap-bytes c extension-number xid)
    (extension-number (scalar CARD8)) (opcode (scalar CARD8 3))
    (_ (pad 2)) (xid (scalar CARD32)) (_ (align 4)))
  (define-reply
    query-client-pixmap-bytes
    (_ (pad 1))
    (bytes
      (scalar CARD32)
      xsb-res:query-client-pixmap-bytes-bytes)
    (bytes-overflow
      (scalar CARD32)
      xsb-res:query-client-pixmap-bytes-bytes-overflow))
  (define XCB_RES_QUERY_CLIENT_IDS 4)
  (define (putreq-res:query-client-ids p extension-number
           num-specs specs)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p extension-number) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 4) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 num-specs)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [sum-len (let lp ([x* specs] [sum-len 0] [i 0])
                      (if (null? x*)
                          sum-len
                          (let ([len ((offset
                                        (fx+ offset
                                             (put-res:client-id-spec
                                               p
                                               (car x*)))))])
                            (lp (cdr x*) (fx+ sum-len len) (fx+ i 1)))))]
           [offset (fx+ offset sum-len)]
           [offset^ (fxand (fx+ offset 3) -4)]
           [_ (begin
                (put-bytevector p (make-bytevector (fx- offset^ offset) 0))
                #f)]
           [offset offset^])
      offset))
  (define (xsb-res:query-client-ids c extension-number
           num-specs specs)
    (let ([x (call-with-bytevector-output-port
               (lambda (p)
                 (putreq-res:query-client-ids
                   p
                   extension-number
                   num-specs
                   specs)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request
    (xsb-res:query-client-ids
      c
      extension-number
      num-specs
      specs)
    (extension-number (scalar CARD8)) (opcode (scalar CARD8 4))
    (_ (pad 2)) (num-specs (scalar CARD32))
    (specs (list (struct ClientIdSpec) num-specs))
    (_ (align 4)))
  (define-reply query-client-ids (_ (pad 1))
    (num-ids (scalar CARD32) xsb-res:query-client-ids-num-ids)
    (_ (pad 20))
    (ids (list (struct ClientIdValue) num-ids)
         xsb-res:query-client-ids-ids))
  (define XCB_RES_QUERY_RESOURCE_BYTES 5)
  (define (putreq-res:query-resource-bytes p extension-number
           client num-specs specs)
    (let* ([offset 0]
           [bvtemp (make-bytevector 8)]
           [_ (begin (put-u8 p extension-number) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-u8 p 5) #f)]
           [offset (fx+ offset 1)]
           [_ (begin (put-bytevector p (make-bytevector 2 0)) #f)]
           [offset (fx+ offset 2)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 client)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [_ (begin
                (bytevector-u32-native-set! bvtemp 0 num-specs)
                (put-bytevector p bvtemp 0 4)
                #f)]
           [offset (fx+ offset 4)]
           [sum-len (let lp ([x* specs] [sum-len 0] [i 0])
                      (if (null? x*)
                          sum-len
                          (let ([len ((offset
                                        (fx+ offset
                                             (put-res:resource-id-spec
                                               p
                                               (car x*)))))])
                            (lp (cdr x*) (fx+ sum-len len) (fx+ i 1)))))]
           [offset (fx+ offset sum-len)]
           [offset^ (fxand (fx+ offset 3) -4)]
           [_ (begin
                (put-bytevector p (make-bytevector (fx- offset^ offset) 0))
                #f)]
           [offset offset^])
      offset))
  (define (xsb-res:query-resource-bytes c extension-number
           client num-specs specs)
    (let ([x (call-with-bytevector-output-port
               (lambda (p)
                 (putreq-res:query-resource-bytes p extension-number client
                   num-specs specs)))])
      (bytevector-u16-native-set!
        x
        2
        (fxdiv (bytevector-length x) 4))
      (put-bytevector (xsb-connection-oport c) x)))
  (define-request
    (xsb-res:query-resource-bytes c extension-number client
      num-specs specs)
    (extension-number (scalar CARD8)) (opcode (scalar CARD8 5))
    (_ (pad 2)) (client (scalar CARD32))
    (num-specs (scalar CARD32))
    (specs (list (struct ResourceIdSpec) num-specs))
    (_ (align 4)))
  (define-reply query-resource-bytes (_ (pad 1))
    (num-sizes
      (scalar CARD32)
      xsb-res:query-resource-bytes-num-sizes)
    (_ (pad 20))
    (sizes
      (list (struct ResourceSizeValue) num-sizes)
      xsb-res:query-resource-bytes-sizes)))
