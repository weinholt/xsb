(library (xsb private syntax)
  (export
    define-struct
    define-request
    define-reply)
  (import (rnrs))

(begin
  (define-syntax define-struct
    (syntax-rules ()
      ((_ struct-name f* ...)
       (define-struct-aux (f* ...) ()))))
  (define-syntax define-struct-aux
    (syntax-rules ()
      ((_ (f) (acc* ...))
       (define-struct-field-ref f (acc* ...)))
      ((_ (f f* ...) (acc* ...))
       (begin
         (define-struct-field-ref f (acc* ...))
         (define-struct-aux (f* ...) (f acc* ...))))))
  (define-syntax type-size
    (syntax-rules (pad align scalar struct list xidtype
                       BYTE BOOL CARD8 CARD16 CARD32
                       INT16)
      ((_ pad n) n)
      ((_ scalar CARD8) 1)
      ((_ scalar BYTE) 1)
      ((_ scalar BOOL) 1)
      ((_ scalar CARD16) 2)
      ((_ scalar INT16) 2)
      ((_ scalar CARD32) 4)
      ((_ xidtype _) 4)
      ((_ (list simpler-type len)) (fx* (type-size simpler-type) len))))
  (define-syntax field-offset
    (syntax-rules (pad align)
      ((_ f*)
       (field-offset f* 0))
      ((_ () acc)
       acc)
      ((_ ((pad n) f* ...) acc)
       (field-offset (f* ...) (fx+ n acc)))
      ((_ ((align n) f* ...) acc)
       (field-offset (f* ...) (fxand (fx+ acc (fx- n 1)) (fx- n))))

      ((_ ((_ (type ...) . _) f* ...) acc)
       (field-offset (f* ...) (fx+ (type-size type ...) acc)))))
  (define-syntax field-ref
    (syntax-rules (scalar struct list xidtype xidunion
                          BYTE BOOL CARD8 CARD16 CARD32
                          INT16)
      ((_ x (scalar BYTE) offset)
       ;; FIXME: is BYTE signed?
       (bytevector-u8-ref x offset))
      ((_ x (scalar BOOL) offset)
       (not (eqv? 0 (bytevector-u8-ref x offset))))

      ((_ x (scalar CARD8) offset)
       (bytevector-u8-ref x offset))
      ((_ x (scalar CARD16) offset)
       (bytevector-u16-native-ref x offset))
      ((_ x (scalar INT16) offset)
       (bytevector-s16-native-ref x offset))
      ((_ x (scalar CARD32) offset)
       (bytevector-u32-native-ref x offset))
      ((_ x (xidtype _) offset)
       (bytevector-u32-native-ref x offset))

      ((_ x (struct _) offset)
       (error #f "No ref for structs"))
      ((_ x (list _ len) offset)
       (error #f "No ref for lists" 'len))))

  (define-syntax define-struct-field-ref
    (syntax-rules (pad align scalar list xidtype)
      ((_ (field-name (pad . _)) (acc* ...))
       (define dummy #f))
      ((_ (field-name (align . _)) (acc* ...))
       (define dummy #f))

      ((_ (field-name (type ...) ref-name) (acc* ...))
       (define ref-name
         (lambda (x)
           (field-ref x (type ...) (field-offset (acc* ...))))))))
  #;
  (expand/optimize
   '(define-struct Setup
      (status (scalar CARD8) xsb-setup-status)
      (_ (pad 1))
      (protocol-major-version (scalar CARD16)
                              xsb-setup-protocol-major-version)
      (protocol-minor-version (scalar CARD16)
                              xsb-setup-protocol-minor-version)
      (length (scalar CARD16) xsb-setup-length)
      (release-number (scalar CARD32) xsb-setup-release-number)
      (resource-id-base (scalar CARD32)
                        xsb-setup-resource-id-base)
      (resource-id-mask (scalar CARD32)
                        xsb-setup-resource-id-mask)
      (motion-buffer-size (scalar CARD32)
                          xsb-setup-motion-buffer-size)
      (vendor-len (scalar CARD16) xsb-setup-vendor-len)
      (maximum-request-length (scalar CARD16)
                              xsb-setup-maximum-request-length)
      (roots-len (scalar CARD8) xsb-setup-roots-len)
      (pixmap-formats-len (scalar CARD8)
                          xsb-setup-pixmap-formats-len)
      (image-byte-order (scalar CARD8) xsb-setup-image-byte-order)
      (bitmap-format-bit-order (scalar CARD8)
                               xsb-setup-bitmap-format-bit-order)
      (bitmap-format-scanline-unit (scalar CARD8)
                                   xsb-setup-bitmap-format-scanline-unit)
      (bitmap-format-scanline-pad (scalar CARD8)
                                  xsb-setup-bitmap-format-scanline-pad)
      (min-keycode (scalar CARD8) xsb-setup-min-keycode)
      (max-keycode (scalar CARD8) xsb-setup-max-keycode)
      (_ (pad 4))
      (vendor (list (scalar char) vendor-len) xsb-setup-vendor)
      (_ (align 4))
      (pixmap-formats (list (struct FORMAT) pixmap-formats-len)
                      xsb-setup-pixmap-formats)
      (roots (list (struct SCREEN) roots-len) xsb-setup-roots))
   #;
   '(define-struct SCREEN
      (root (xidtype WINDOW) screen-root)
      (default-colormap (xidtype COLORMAP) screen-default-colormap)
      (white-pixel (scalar CARD32) screen-white-pixel)
      (black-pixel (scalar CARD32) screen-black-pixel)
      (current-input-masks (scalar CARD32) screen-current-input-masks)
      (width-in-pixels (scalar CARD16) screen-width-in-pixels)
      (height-in-pixels (scalar CARD16) screen-height-in-pixels)
      (width-in-millimeters (scalar CARD16) screen-width-in-millimeters)
      (height-in-millimeters (scalar CARD16) screen-height-in-millimeters)
      (min-installed-maps (scalar CARD16) screen-min-installed-maps)
      (max-installed-maps (scalar CARD16) screen-max-installed-maps)
      (root-visual (scalar CARD32) screen-root-visual)
      (backing-stores (scalar BYTE) screen-backing-stores)
      (save-unders (scalar BOOL) screen-save-unders)
      (root-depth (scalar CARD8) screen-root-depth)
      (allowed-depths-len (scalar CARD8) screen-allowed-depths-len)
      (allowed-depths (list (struct DEPTH) allowed-depths-len)
                      screen-allowed-depths))))



(define-syntax define-request
  (syntax-rules ()
    ((_ (name arg* ...) f* ...)
     (define foo #f))))

(define-syntax define-reply
  (syntax-rules ()
    ((_ name f* ...)
     (define foo #f))))

  )