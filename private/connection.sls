#!r6rs

(library (xsb private connection)
  (export
    make-xsb-connection xsb-connection?
    xsb-connection-oport
    xsb-connection-iport
    xsb-connection-setup
    xsb-connection-setup-set!
    xsb-connection-high-sequence
    xsb-connection-high-sequence-set!
    xsb-connection-id
    xsb-connection-id-set!)
  (import
    (rnrs))

(define-record-type xsb-connection
  (sealed #t)
  ;; (opaque #t)
  (fields iport oport screen
          (mutable setup)
          (mutable high-sequence)
          (mutable id))
  (protocol
   (lambda (p)
     (lambda (iport oport screen)
       (p iport oport screen
          #f 0 0)))))


  )