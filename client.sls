#!r6rs

;;; X client

(library (xsb client)
  (export
    xsb-connection?
    xsb-connection-setup

    xsb-connect-to-ports
    xsb-connect
    xsb-disconnect

    xsb-generate-id!

    xsb-flush
    xsb-read-message)
  (import
    (rnrs)
    (srfi :98 os-environment-variables)
    ;; (srfi :106)
    (xsb local)
    (xsb authority)
    (except (xsb xproto) xsb-screen-root xsb-screen-root-visual)
    (xsb private connection))

(define protocol-major-version 11)      ;X11
(define protocol-minor-version 0)

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

#;
(define (no-sequence-number? code)
  ;; XXX: Hardcoded, matches everything with no-sequence-number="true"
  (eqv? code 11))

;; The syntax for DISPLAY: hostname:displaynumber.screennumber
;; hostname:number   TCP to hostname port 6000+number
;; :number           the fastest mechanism (e.g. /tmp/.X11-unix/X0)
;; hostname::number  DECnet
;; The .screennumber part can be omitted.

;; Parse a DISPLAY string and return three values: connection method
;; (decnet, local or tcp), service or file name, and default screen
;; number. Pretty easy to crash with bad input.
(define (parse-display display)
  (define (pdisplay s)
    (car (string-split s #\.)))
  (define (pscreen s)
    (let ((s (string-split s #\.)))
      (if (null? (cdr s))
          0
          (string->number (cadr s)))))
  (define (parse-ipv6 display)
    (let* ((d (string-split (substring display 1 (string-length display)) #\]))
           (addr (car d))
           (disp (substring (cadr d) 1 (string-length (cadr d)))))
      (values 'TCP/IP addr
              (number->string (+ 6000 (string->number (pdisplay disp))))
              (pscreen disp))))
  (let ((d (string-split display #\:)))
    (cond ((and (>= (string-length display) 7)
                (char=? #\[ (string-ref display 0)))
           (parse-ipv6 display))
          ((= (length d) 3)
           ;; DECnet. It is here for completeness.
           (values 'DECnet (car d)
                   (pdisplay (caddr d))
                   (pscreen (caddr d))))
          ((and (= (length d) 2) (string=? (car d) ""))
           (values 'Local
                   (string-append "/tmp/.X11-unix/X" (pdisplay (cadr d)))
                   #f
                   (pscreen (cadr d))))
          ((and (= (length d) 2)
                (string->number (pdisplay (cadr d))))
           => (lambda (dispnum)
                (values 'TCP/IP (car d)
                        (number->string (+ 6000 dispnum))
                        (pscreen (cadr d)))))
          (else
           (error 'xsb-connect "Invalid display string" display)))))

(define (find-authority auths method name service)
  ;; TODO: Pick the right authority record. This is only matching
  ;; protocols with address families, but we should match on the
  ;; hostname. For local, do we need to know the system's hostname?
  (find (case method
          ((Local)
           (lambda (auth)
             (eq? (authority-family auth) 'Local)))
          ((TCP/IP)
           (lambda (auth)
             (memq (authority-family auth) '(Internet Internet6))))
          (else
           (lambda (auth)
             #t)))
        auths))

(define (xsb-flush c)
  (flush-output-port (xsb-connection-oport c)))

;; Read an error, reply or event from the connection and parse it.
(define (xsb-read-message c)
  (let* ((temp (make-bytevector 8))
         (n (get-bytevector-n! (xsb-connection-iport c) temp 0 8)))
    (if (or (eof-object? n) (fx<? n 8))
        (eof-object)
        (let ((code (bytevector-u8-ref temp 0))
              (reply-length (bytevector-u32-native-ref temp 4)))
          (let* ((len (if (eqv? code 1)
                          (fx+ 32 (fx* reply-length 4))
                          32))
                 (bv (make-bytevector len)))
            (bytevector-copy! temp 0 bv 0 8)
            (write (list 'code code 'reply-length reply-length 'len len 'temp temp))
            (newline)
            (let ((n (get-bytevector-n! (xsb-connection-iport c) bv 8 (fx- len 8))))
              (if (eqv? n (fx- len 8))
                  bv
                  (error 'xsb-read-message! "Short read" n))))))))

;; The setup response is a bit different from everything else.
(define (xsb-read-setup-response c)
  (let* ((temp (make-bytevector 8))
         (n (get-bytevector-n! (xsb-connection-iport c) temp 0 8)))
    (if (or (eof-object? n) (fx<? n 8))
        (error 'xsb-read-setup-response "Short initial read" n)
        (let* ((len (fx* 4 (bytevector-u16-native-ref temp 6)))
               (bv (make-bytevector (fx+ 8 len))))
          (bytevector-copy! temp 0 bv 0 8)
          (let ((n (get-bytevector-n! (xsb-connection-iport c) bv 8 len)))
            (if (eqv? n len)
                bv
                (error 'xsb-read-setup-response "Short read" n len)))))))

;; Connect to the X server over the given input and output ports,
;; optionally with authentication
(define xsb-connect-to-ports
  (case-lambda
    ((iport oport)
     (xsb-connect-to-ports iport oport #f))
    ((iport oport auth)
     (xsb-connect-to-ports iport oport auth 0))
    ((iport oport auth screen-number)
     (let ((c (make-xsb-connection iport oport screen-number))
           (auth (or auth (make-authority #f #f #f #vu8() #vu8()))))
       (let ((authname (string->utf8 (authority-name auth)))
             (authdata (authority-data auth))
             (byte-order (case (native-endianness)
                           ((little) (char->integer #\l))
                           ((big) (char->integer #\B))
                           (else (error 'open-display
                                        "Unsupported native endianness"
                                        (native-endianness))))))
         (put-setup-request oport byte-order
                            protocol-major-version
                            protocol-minor-version
                            0 0 #vu8() #vu8())
         (xsb-flush c)
         (let ((setup (xsb-read-setup-response c)))
           (case (xsb-setup-status setup)
             ((1)
              (xsb-connection-setup-set! c setup)
              c)
             ((2)
              (put-setup-request oport byte-order
                                 protocol-major-version
                                 protocol-minor-version
                                 (bytevector-length authname)
                                 (bytevector-length authdata)
                                 authname authdata)
              (let ((setup (xsb-read-setup-response c)))
                (case (xsb-setup-status setup)
                  ((1)
                   (xsb-connection-setup-set! c setup)
                   c)
                  (else
                   (error 'xsb-connect-to-ports "Authentication failed"
                          iport oport auth setup)))))
             (else
              (error 'xsb-connect-to-ports "Authentication failed"
                     iport oport auth setup)))))))))

;; Connect to an X display, optionally given a display name
(define xsb-connect
  (case-lambda
    (()
     (xsb-connect #f))
    ((display)
     (let ((display (or display (get-environment-variable "DISPLAY"))))
       (unless (string? display)
         (error 'open-display
                "No display given (try setting the environment variable DISPLAY)"))
       (let-values ([(method name service screen-number) (parse-display display)])
         (let ((auth (find-authority (xsb-read-authority (xsb-find-authority-file))
                                     method name service)))
           (let-values ([(i o) (case method
                                 ((Local) (local-connect name))
                                 ;; ((Internet Internet6) (tcp-connect name service))
                                 (else
                                  (error 'xsb-connect
                                         "Unsupported method" display)))])
             (xsb-connect-to-ports i o auth screen-number))))))))

(define (xsb-disconnect c)
  (close-port (xsb-connection-iport c))
  (close-port (xsb-connection-oport c)))

;; The server has assigned the client a contiguous range of resource
;; ids. Generate the next id.
(define (xsb-generate-id! c)
  (let ((setup (xsb-connection-setup c))
        (id (xsb-connection-id c)))
    (let ((base (xsb-setup-resource-id-base setup))
          (mask (xsb-setup-resource-id-mask setup)))
      (let ((id^ (bitwise-arithmetic-shift-left id (bitwise-first-bit-set mask))))
        (unless (= id^ (bitwise-and mask id^))
          (error 'xsb-generate-id! "Out of resource ids" id^ mask))
        (xsb-connection-id-set! c (+ id 1))
        (fxior base id^))))))
