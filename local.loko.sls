;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2019 Göran Weinholt
#!r6rs

;;; Local ("Unix") sockets

(library (xsb local)
  (export
    local-connect
    #;local-listen
    )
  (import
    (rnrs (6))
    (only (loko) port-file-descriptor-set!)
    (loko system fibers)
    (loko system unsafe)
    (loko arch amd64 linux-syscalls)
    (loko arch amd64 linux-numbers))

(define (filename->sockaddr_un who fn)
  (unless (string? fn)
    (assertion-violation who "Expected a string" fn))
  (string-for-each
   (lambda (c)
     (when (eqv? c #\nul)
       (raise (condition
               (make-who-condition who)
               (make-i/o-filename-error fn)
               (make-message-condition "Unrepresentable filename")
               (make-irritants-condition (list fn))))))
   fn)
  (let ((addr (make-bytevector sizeof-sockaddr_un 0))
        (filename-bv (string->utf8 fn)))
    (bytevector-u16-native-set! addr offsetof-sockaddr_un-sun_family AF_LOCAL)
    ;; XXX: Linux can handle a missing NUL character
    (unless (fx<? (bytevector-length filename-bv)
                  (fx- sizeof-sockaddr_un offsetof-sockaddr_un-sun_path))
      (assertion-violation who "The filename is too long" fn))
    (bytevector-copy! filename-bv 0
                      addr offsetof-sockaddr_un-sun_path
                      (bytevector-length filename-bv))
    addr))

;; TODO: out-of-band transmission of file descriptors

(define (local-connect filename)
  (define NULL 0)
  (define fd
    (sys_socket AF_LOCAL (fxior SOCK_STREAM SOCK_CLOEXEC SOCK_NONBLOCK) 0))
  (define (sendto fd buf start count)
    (define flags 0)
    (assert (fx<=? 0 (fx+ start count) (bytevector-length buf)))
    (sys_sendto fd (fx+ (bytevector-address buf) start) count
                flags NULL 0
                (lambda (errno)
                  (cond ((eqv? errno EAGAIN)
                         (wait-for-writable fd)
                         (sendto fd buf start count))
                        ((eqv? errno EINTR)
                         (sendto fd buf start count))
                        (else
                         (raise (condition
                                 (make-syscall-error 'sendto errno)
                                 (make-irritants-condition (list fd)))))))))
  (define (recvfrom fd buf start count)
    (assert (fx<=? 0 (fx+ start count) (bytevector-length buf)))
    (sys_recvfrom fd (bytevector-address buf) (bytevector-length buf)
                  0 NULL 0
                  (lambda (errno)
                    (cond ((eqv? errno EAGAIN)
                           (wait-for-readable fd)
                           (recvfrom fd buf start count))
                          ((eqv? errno EINTR)
                           (recvfrom fd buf start count))
                          (else
                           (raise (condition
                                   (make-syscall-error 'recvfrom errno)
                                   (make-irritants-condition (list fd)))))))))
  (define (read! bv start count)
    (recvfrom fd bv start count))
  (define (write! bv start count)
    (sendto fd bv start count))
  (define half-closed #f)
  (define (close-read)
    (cond (half-closed
           (sys_close fd))
          (else
           (set! half-closed #t)
           (sys_shutdown fd SHUT_RD))))
  (define (close-write)
    (cond (half-closed
           (sys_close fd))
          (else
           (set! half-closed #t)
           (sys_shutdown fd SHUT_WR))))
  (let ((addr (filename->sockaddr_un 'local-connect filename)))
    (let retry ()
      (sys_connect fd (bytevector-address addr) (bytevector-length addr)
                   (lambda (errno)
                     (cond ((eqv? errno EAGAIN)
                            ;; FIXME: Is this actually correct?
                            ;; Manpages are unclear.
                            (wait-for-writable fd)
                            (retry))
                           ((eqv? errno EINTR)
                            (retry))
                           (else
                            (raise (condition
                                    (make-syscall-error 'connect errno)
                                    (make-irritants-condition (list fd filename))))))))))
  (let* ((id (string-append "local-connect " filename))
         (i (make-custom-binary-input-port id read! #f #f close-read))
         (o (make-custom-binary-output-port id write! #f #f close-write))) 
    (port-file-descriptor-set! i fd)
    (port-file-descriptor-set! o fd)
    (values i o))))
