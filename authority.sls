
;;; X authority

(library (xsb authority)
  (export
    xsb-read-authority
    xsb-find-authority-file

    make-authority authority?
    authority-family
    authority-address
    authority-number
    authority-name
    authority-data)
  (import
    (rnrs)
    (struct pack)
    (srfi :98 os-environment-variables))

(define-record-type authority
  (sealed #t)
  ;; name and data can also be empty, which means "no explicit
  ;; authorization".
  (fields family                        ;commonly: Local, Internet, Internet6
          address                       ;bytevector: hostname or address
          number                        ;string: display number, e.g. "0"
          name                          ;"MIT-MAGIC-COOKIE-1"
          data))                        ;bytevector

(define (xsb-read-authority filename)
  ;; Read an Xauthority file.
  (define (lookup-family f)
    (define families
      '((0 . Internet)
        (1 . DECnet)
        (2 . Chaos)
        (5 . ServerInterpreted)
        (6 . Internet6)
        (252 . LocalHost)
        (253 . Krb5Principal)
        (254 . Netname)
        (256 . Local)
        (65535 . Wild)))
    (cond ((assv f families) => cdr)
          (else f)))
  (define (read-bv p)
    (let* ((len (get-unpack p "!S"))
           (bv (get-bytevector-n p len)))
      (when (or (eof-object? bv) (fx<? (bytevector-length bv) len))
        (error 'get-xauthority "Premature end of file" p))
      bv))
  (call-with-port (open-file-input-port filename)
    (lambda (p)
      (let lp ()
        (if (port-eof? p)
            '()
            (let* ((family (get-unpack p "!S"))
                   (address (read-bv p))
                   (number (read-bv p))
                   (name (read-bv p))
                   (data (read-bv p)))
              (let ((auth (make-authority (lookup-family family)
                                          address
                                          (utf8->string number)
                                          (utf8->string name)
                                          data)))
                (cons auth (lp)))))))))

(define (xsb-find-authority-file)
  (let* ((home (get-environment-variable "HOME"))
         (fn* (list (get-environment-variable "XAUTHORITY")
                    (and home (string-append home "/.Xauthority"))
                    ;; Feel free to fill in with every obscure variant
                    ;; you can find
                    "SYS$LOGIN:DECW$XAUTHORITY.DECW$XAUTH")))
    (let lp ((fn* fn*))
      (cond ((null? fn*)
             (error 'find-authority
                    "Could not find the Xauthority file" fn*))
            ((and (car fn*) (file-exists? (car fn*)))
             (car fn*))
            (else
             (lp (cdr fn*))))))))
