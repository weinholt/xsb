#!/usr/bin/env scheme-script

#!r6rs

(import
  (rnrs)
  (xsb client)
  (except (xsb xproto) xsb-setup-roots-iterator
          xsb-screen-root xsb-screen-root-visual))

(define (xsb-setup-roots-iterator setup) ;FIXME: autogen
  (lambda ()
    0))

(define (xsb-screen-root screen)        ;FIXME: autogen
  #x000006ad)

(define (xsb-screen-root-visual screen) ;FIXME: autogen
  #x00000021)


(define c (xsb-connect ":1"))

(define screen ((xsb-setup-roots-iterator (xsb-connection-setup c))))

(define win (xsb-generate-id! c))

;; TODO: /checked should be a variant that waits for a reply. In case
;; there is no reply expected, send a trivial request that provokes a
;; reply...
(xsb-create-window c
                   0
                   win
                   (xsb-screen-root screen)
                   0 0
                   250 150
                   10
                   XCB_WINDOW_CLASS_INPUT_OUTPUT
                   (xsb-screen-root-visual screen)
                   0 '())

(let ((title (string->utf8 "test-client.sps"))
      (title-icon (string->utf8 "test")))
  (xsb-change-property c XCB_PROP_MODE_REPLACE win
                       XCB_ATOM_WM_NAME
                       XCB_ATOM_STRING 8
                       (bytevector-length title) title)
  (xsb-change-property c XCB_PROP_MODE_REPLACE win
                       XCB_ATOM_WM_ICON_NAME
                       XCB_ATOM_STRING 8
                       (bytevector-length title-icon) title-icon))

(xsb-map-window c win)

(xsb-flush c)

(let lp ()
  (write (xsb-read-message c))
  (newline)
  (lp))