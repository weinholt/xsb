EXTENSIONS=$(wildcard xcb/proto/src/*.xml)
GENERATED=$(patsubst xcb/proto/src/%.xml,generated/%.sls,$(EXTENSIONS))

all: $(GENERATED)

$(GENERATED): generated/%.sls: xcb/proto/src/%.xml tools/xsbgen.sps
	.akku/env tools/xsbgen.sps --r6rs $(dir $<) $< -o $@

extraclean:
	rm -f generated/*.sls
